#include <float.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

/*********
 * Batch *
 *********/

struct Batch {                 // A batch to process
    unsigned int num_machines; // The number of available machines
    unsigned int num_jobs;     // The number of jobs to process
    float *durations;          // The duration of each job
};

/**
 * Loads the batch data from stdin.
 *
 * @param batch  The batch in which the data is stored
 */
void load_batch(struct Batch *batch) {
    scanf("%d %d\n", &batch->num_machines, &batch->num_jobs);
    batch->durations = malloc(batch->num_jobs * sizeof(float));
    for (unsigned int j = 0; j < batch->num_jobs; ++j) {
        scanf("%f\n", &batch->durations[j]);
    }
}

/**
 * Prints the given batch to stdout.
 *
 * @param batch  The batch to print
 */
void print_batch(const struct Batch *batch) {
    printf("A batch of %d jobs to be processed by %d machines",
           batch->num_jobs, batch->num_machines);
    printf(" whose durations are\n");
    for (unsigned int j = 0; j < batch->num_jobs; ++j) {
        printf("  t(j%d) = %.2f\n", j, batch->durations[j]);
    }
}

/**
 * Frees the given batch.
 *
 * @param batch  The batch to free
 */
void free_batch(struct Batch *batch) {
    free(batch->durations);
}

/************
 * Schedule *
 ************/

struct Schedule {                   // A schedule for processing the batch
    const struct Batch *batch;      // The scheduled batch
    unsigned int *assigned_machine; // The machine assigned to each job
    float *duration_by_machine;     // The total duration for each machine
};

/**
 * Computes the minimum duration of a machine in a schedule.
 *
 * @param schedule  The schedule
 * @return          The duration
 */
float min_duration(const struct Schedule *schedule) {
    float duration = schedule->duration_by_machine[0];
    for (unsigned int m = 1; m < schedule->batch->num_machines; ++m) {
        float d = schedule->duration_by_machine[m];
        duration = d < duration ? d : duration;
    }
    return duration;
}

/**
 * Computes the maximum duration of a machine in a schedule.
 *
 * @param schedule  The schedule
 * @return          The duration
 */
float max_duration(const struct Schedule *schedule) {
    float duration = schedule->duration_by_machine[0];
    for (unsigned int m = 1; m < schedule->batch->num_machines; ++m) {
        float d = schedule->duration_by_machine[m];
        duration = d > duration ? d : duration;
    }
    return duration;
}

/**
 * Number of machines with maximum load in a schedule.
 *
 * @param schedule  The schedule
 * @return          The number of machine with maximum load
 */
unsigned int num_machines_max_load(const struct Schedule *schedule) {
    float max_duration = schedule->duration_by_machine[0];
    unsigned int num_machines;
    for (unsigned int m = 1; m < schedule->batch->num_machines; ++m) {
        float d = schedule->duration_by_machine[m];
        if (d > max_duration) {
            max_duration = d;
            num_machines = 1;
        } else if (d == max_duration) {
            ++num_machines;
        }
    }
    return num_machines;
}

void update_schedule_durations(struct Schedule *schedule) {
    for (unsigned int m = 0; m < schedule->batch->num_machines; ++m) {
        schedule->duration_by_machine[m] = 0.0;
    }
    for (unsigned int j = 0; j < schedule->batch->num_jobs; ++j) {
        unsigned int m = schedule->assigned_machine[j];
        schedule->duration_by_machine[m] += schedule->batch->durations[j];
    }
}

/**
 * Initializes a schedule from a given batch.
 *
 * @param schedule  The schedule to initialize
 * @param batch     The batch to process
 */
void alloc_schedule(struct Schedule *schedule,
                    const struct Batch *batch) {
    schedule->batch = batch;
    schedule->assigned_machine =
        malloc(batch->num_jobs * sizeof(unsigned int));
    schedule->duration_by_machine =
        malloc(batch->num_machines * sizeof(float));
}

/**
 * Initializes a random schedule from a given batch.
 *
 * @param schedule  The schedule to initialize
 * @param batch     The batch to process
 */
void initialize_random_schedule(struct Schedule *schedule,
                                const struct Batch *batch) {
    alloc_schedule(schedule, batch);
    for (unsigned int j = 0; j < batch->num_jobs; ++j) {
        unsigned int m = rand() % batch->num_machines;
        schedule->assigned_machine[j] = m;
    }
    update_schedule_durations(schedule);
}

/**
 * Prints the given schedule to stdout.
 *
 * @param schedule  The schedule to print
 */
void print_schedule(const struct Schedule *schedule) {
    printf("A schedule of %d jobs dispatched onto %d machines",
           schedule->batch->num_jobs, schedule->batch->num_machines);
    printf(" as follows:\n");
    for (unsigned int j = 0; j < schedule->batch->num_jobs; ++j) {
        printf("  Job #%d is assigned to machine #%d\n",
               j, schedule->assigned_machine[j]);
    }
    for (unsigned int m = 0; m < schedule->batch->num_machines; ++m) {
        printf("  Machine #%d's duration is %f\n",
               m, schedule->duration_by_machine[m]);
    }
}

/**
 * Frees the given schedule.
 *
 * @param schedule  The schedule to free
 */
void free_schedule(struct Schedule *schedule) {
    free(schedule->assigned_machine);
}

/***************************
 * Approximation algorithm *
 ***************************/

/**
 * Moves the given job to the given machine in the schedule.
 *
 * @param schedule  The schedule
 * @param j         The job to move
 * @param m         The machine on which the job is moved
 */
void move_job(struct Schedule *schedule,
              unsigned int j, unsigned int m) {
    unsigned int m1 = schedule->assigned_machine[j];
    if (m1 != m) {
        schedule->assigned_machine[j] = m;
        schedule->duration_by_machine[m] +=
            schedule->batch->durations[j];
        schedule->duration_by_machine[m1] -=
            schedule->batch->durations[j];
    }
}

/**
 * Improves, if possible, the given schedule by moving one job.
 *
 * A schedule is improved if its maximum duration can be made smaller by moving
 * a single job, or if it is possible to reduce the number of machines that
 * have a maximum load.
 *
 * @param schedule  The schedule to improve
 * @return          true if the schedule has been improved
 *                  false otherwise
 */
bool improve_schedule(struct Schedule *schedule) {
    float min_d = min_duration(schedule);
    float max_d = max_duration(schedule);
    unsigned int num_machines = num_machines_max_load(schedule);
    const struct Batch *batch = schedule->batch;
    for (unsigned int m1 = 0; m1 < batch->num_machines; ++m1) {
        for (unsigned int m2 = 0; m2 < batch->num_machines; ++m2) {
            if (schedule->duration_by_machine[m1] == max_d &&
                schedule->duration_by_machine[m2] == min_d) {
                for (unsigned int j = 0; j < batch->num_jobs; ++j) {
                    if (schedule->assigned_machine[j] == m1) {
                        move_job(schedule, j, m2);
                        float max_d2 = max_duration(schedule);
                        if (max_d2 < max_d || (max_d2 == max_d &&
                            num_machines_max_load(schedule) < num_machines)) {
                            return true;
                        }
                        move_job(schedule, j, m1);
                    }
                }
            }
        }
    }
    return false;
}

/*******************
 * Naive algorithm *
 *******************/

struct Search {               // A naive search
    struct Schedule schedule; // The current schedule
    float optimal_duration;   // The optimal duration found so far
    bool verbose;             // If true, prints trace to stdout
};

/**
 * Recursively computes and optimal schedule.
 *
 * @param search  The naive search used to find the schedule
 * @param j       The current unassigned task (i.e. the depth)
 */
void compute_optimal_schedule_recursive(struct Search *search,
                                        unsigned int j) {
    const struct Batch *batch = search->schedule.batch;
    if (j < batch->num_jobs) {
        for (unsigned int m = 0; m < batch->num_machines; ++m) {
            search->schedule.assigned_machine[j] = m;
            compute_optimal_schedule_recursive(search, j + 1);
        }
    } else if (j == batch->num_jobs) {
        update_schedule_durations(&search->schedule);
        float duration = max_duration(&search->schedule);
        if (duration < search->optimal_duration) {
            search->optimal_duration = duration;
            if (search->verbose) {
                printf("Improved optimal duration to %f\n", duration);
            }
        }
    }
}

/**
 * Naively computes an optimal solution.
 *
 * @param batch    The batch to process
 * @param verbose  If true, prints the trace to stdout
 */
float optimal_duration(const struct Batch *batch,
                       bool verbose) {
    struct Search search;
    search.verbose = verbose;
    search.optimal_duration = FLT_MAX;
    alloc_schedule(&search.schedule, batch);
    compute_optimal_schedule_recursive(&search, 0);
    free_schedule(&search.schedule);
    return search.optimal_duration;
}

/********
 * Main *
 ********/

int main(int argc, char *argv[]) {
    bool verbose = false;
    bool quiet = false;
    char algorithm = 'a';
    float duration;
    if (argc == 2) {
        if (argv[1][0] == 'v') verbose = true;
        else if (argv[1][0] == 'q') quiet = true;
        algorithm = argv[1][1];
    }
    srand(time(NULL));
    struct Batch batch;
    load_batch(&batch);
    if (verbose) print_batch(&batch);

    if (algorithm == 'a') {
        struct Schedule schedule;
        initialize_random_schedule(&schedule, &batch);
        if (verbose) print_schedule(&schedule);
        while (improve_schedule(&schedule)) {
            if (verbose) print_schedule(&schedule);
        }
        duration = max_duration(&schedule);
        free_schedule(&schedule);
    } else if (algorithm == 'n') {
        duration = optimal_duration(&batch, verbose);
    }

    if (!quiet) printf("%f\n", duration);
    free_batch(&batch);
    return 0;
}
